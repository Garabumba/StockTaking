docker-compose up -d django

docker-compose exec django bash

python manage.py makemigrations

python manage.py migrate

python manage.py loaddata users/fixtures/user.json

python manage.py loaddata users/fixtures/task_status.json

python manage.py loaddata home/fixtures/technique_status.json

python manage.py loaddata home/fixtures/technique_type.json

python manage.py loaddata home/fixtures/university.json

python manage.py loaddata home/fixtures/audience_type.json

python manage.py loaddata home/fixtures/color_printing.json

python manage.py loaddata home/fixtures/print_type.json

python manage.py loaddata home/fixtures/projector_type.json

python manage.py loaddata home/fixtures/vendor_type.json

python manage.py loaddata home/fixtures/vendor.json